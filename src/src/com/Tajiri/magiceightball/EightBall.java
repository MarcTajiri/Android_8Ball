package com.Tajiri.magiceightball;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Toast;

public class EightBall extends Activity {

	private String[] sayings;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eight_ball);
        sayings = new String[20];
        sayings[0] = "It is certain";
        sayings[1] = "It is decidedly so";
        sayings[2] = "Without a doubt";
        sayings[3] = "Yes - definitely";
        sayings[4] = "You may rely on it";
        sayings[5] = "As I see it, yes";
        sayings[6] = "Most likely";
        sayings[7] = "Outlook good";
        sayings[8] = "Yes";
        sayings[9] = "Signs point to yes";
        sayings[10] = "Reply hazy, try again";
        sayings[11] = "Ask again later";
        sayings[12] = "Better not tell you now";
        sayings[13] = "Cannot predict now";
        sayings[14] = "Concentrate and ask again";
        sayings[15] = "Don't count on it";
        sayings[16] = "My reply is no";
        sayings[17] = "My sources say no";
        sayings[18] = "Outlook not so good";
        sayings[19] = "Very doubtful";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_eight_ball, menu);
        return true;
    }
    
    public void onClick (View view) {
    	switch (view.getId()) {
    	case R.id.eightBallImg:
    		Toast t = Toast.makeText(this, eightBall(), Toast.LENGTH_LONG);
    		t.setGravity(Gravity.CENTER, 0, 0);
    		t.show();
    	}
    }
    	
    private String eightBall() {
    	Random r = new Random();
    	return sayings[r.nextInt(19)];
    }
    
}
